package protobuf

import (
	"bytes"
	"testing"

	"bitbucket.org/50onred/gofifty/test"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/assert"
)

func getMarshaledLen(pb proto.Message) int {
	buf := &bytes.Buffer{}
	codec := ProtobufCodec{}
	codec.Marshal(buf, pb)
	return buf.Len()
}

func TestProtobufCodec(t *testing.T) {
	buf := &bytes.Buffer{}
	codec := ProtobufCodec{}
	msg := &test.Foo{Baz: "test", Quxs: []int32{1, 2, 3, 4, 5}}
	err := codec.Marshal(buf, msg)
	assert.Nil(t, err)
	unmarshalledMsg := &test.Foo{}
	err = codec.Unmarshal(bytes.NewReader(buf.Bytes()), unmarshalledMsg)
	assert.Nil(t, err)
	assert.Equal(t, msg, unmarshalledMsg)
}

func TestProtobufCodec_MarshalDelimited(t *testing.T) {
	buf := &bytes.Buffer{}
	codec := ProtobufCodec{}
	msg := &test.Foo{Baz: "test", Quxs: []int32{1, 2, 3, 4, 5}}
	err := codec.MarshalDelimited(buf, msg)
	assert.Nil(t, err)
	unmarshalledMsg := &test.Foo{}
	data := buf.Bytes()
	lenBuf := proto.EncodeVarint(uint64(getMarshaledLen(msg)))
	assert.True(t, bytes.HasPrefix(data, lenBuf))
	err = codec.Unmarshal(bytes.NewReader(data[len(lenBuf):]), unmarshalledMsg)
	assert.Nil(t, err)
	assert.Equal(t, msg, unmarshalledMsg)
}
