package protobuf

import (
	"bytes"
	"testing"

	"bitbucket.org/50onred/gofifty/test"
	"github.com/stretchr/testify/assert"
)

func TestJSONCodec(t *testing.T) {
	buf := &bytes.Buffer{}
	codec := JSONCodec{}
	msg := &test.Foo{Baz: "test", Quxs: []int32{1, 2, 3, 4, 5}}
	err := codec.Marshal(buf, msg)
	assert.Nil(t, err)
	data := buf.Bytes()
	assert.Equal(t, `{"b":"test","q":[1,2,3,4,5]}`, string(data))
	unmarshalled := &test.Foo{}
	err = codec.Unmarshal(bytes.NewReader(data), unmarshalled)
	assert.Nil(t, err)
	assert.Equal(t, msg, unmarshalled)
}

func TestJSONCodec_MarshalDelimited(t *testing.T) {
	buf := &bytes.Buffer{}
	codec := JSONCodec{}
	msg := &test.Foo{Baz: "test", Quxs: []int32{1, 2, 3, 4, 5}}
	err := codec.MarshalDelimited(buf, msg)
	assert.Nil(t, err)
	data := buf.Bytes()
	assert.Equal(t, "{\"b\":\"test\",\"q\":[1,2,3,4,5]}\n", string(data))
	unmarshalled := &test.Foo{}
	err = codec.Unmarshal(bytes.NewReader(data), unmarshalled)
	assert.Nil(t, err)
	assert.Equal(t, msg, unmarshalled)
}
