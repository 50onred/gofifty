package protobuf

import (
	"io"

	"github.com/golang/protobuf/proto"
)

var protobuf = ProtobufCodec{}
var json = JSONCodec{}

type Codec interface {
	Unmarshal(r io.Reader, pb proto.Message) error
	Marshal(w io.Writer, pb proto.Message) error
	MarshalDelimited(w io.Writer, pb proto.Message) error
}

func get(format string) Codec {
	switch format {
	case "json":
		return json
	case "protobuf":
		return protobuf
	}
	return nil
}

// GetCodec returns a codec. Supported codecs - json and protobuf.
func GetCodec(format string, defaultFormat string) Codec {
	codec := get(format)
	if codec == nil {
		codec = get(defaultFormat)
	}
	return codec
}
