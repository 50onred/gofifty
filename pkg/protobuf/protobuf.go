package protobuf

import (
	"io"
	"io/ioutil"

	"github.com/golang/protobuf/proto"
)

type ProtobufCodec struct{}

// Unmarshal unmarshals a message.
func (ProtobufCodec) Unmarshal(r io.Reader, pb proto.Message) error {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	return proto.UnmarshalMerge(data, pb)
}

// Marshal marshales and writes a message.
func (ProtobufCodec) Marshal(w io.Writer, pb proto.Message) error {
	data, err := proto.Marshal(pb)
	if err != nil {
		return err
	}
	_, err = w.Write(data)
	return err
}

// MarshalDelimited marshales a message and writes the size of the message as a varint before writing the data.
func (ProtobufCodec) MarshalDelimited(w io.Writer, pb proto.Message) error {
	dataBuf, err := proto.Marshal(pb)
	if err != nil {
		return err
	}
	lenBuf := proto.EncodeVarint(uint64(len(dataBuf)))
	outBuf := make([]byte, len(lenBuf)+len(dataBuf))
	copy(outBuf[:len(lenBuf)], lenBuf)
	copy(outBuf[len(lenBuf):], dataBuf)
	_, err = w.Write(outBuf)
	return err
}
