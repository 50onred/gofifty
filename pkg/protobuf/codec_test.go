package protobuf

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCodec(t *testing.T) {
	assert.Nil(t, GetCodec("", ""))
	assert.IsType(t, GetCodec("json", ""), JSONCodec{})
	assert.IsType(t, GetCodec("protobuf", ""), ProtobufCodec{})
	assert.IsType(t, GetCodec("", "protobuf"), ProtobufCodec{})
}
