package protobuf

import (
	"bytes"
	"io"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
)

var jsonUnmarshaler = &jsonpb.Unmarshaler{}
var jsonMarshaler = &jsonpb.Marshaler{EnumsAsInts: true}

type JSONCodec struct{}

// Unmarshal unmarshals a message from JSON.
func (JSONCodec) Unmarshal(r io.Reader, pb proto.Message) error {
	return jsonUnmarshaler.Unmarshal(r, pb)
}

// Marshal marshales and writes a message to JSON.
func (JSONCodec) Marshal(w io.Writer, pb proto.Message) error {
	return jsonMarshaler.Marshal(w, pb)
}

// Marshal marshales, writes a message to JSON and appends a new line character.
func (JSONCodec) MarshalDelimited(w io.Writer, pb proto.Message) error {
	buf := &bytes.Buffer{}
	if err := jsonMarshaler.Marshal(w, pb); err != nil {
		return err
	}
	buf.WriteByte('\n')
	_, err := w.Write(buf.Bytes())
	return err
}
