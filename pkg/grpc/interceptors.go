package grpc

import (
	"errors"
	"fmt"
	"runtime/debug"

	"bitbucket.org/50onred/gofifty/pkg/statsd_extended"

	"github.com/DataDog/datadog-go/statsd"
	"github.com/getsentry/raven-go"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func UnaryInterceptorTimed(g *statsd.Client) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		var resp interface{}
		var err error
		statsd_extended.Timed(g, "grpc.endpoint", []string{"endpoint_name:" + info.FullMethod}, func() {
			resp, err = handler(ctx, req)
		})
		return resp, err
	}
}

func StreamingInterceptorTimed(g *statsd.Client) grpc.StreamServerInterceptor {
	return func(req interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		var err error
		statsd_extended.Timed(g, "grpc.endpoint", []string{"endpoint_name:" + info.FullMethod}, func() {
			err = handler(req, stream)
		})
		return err
	}
}

func RecoveryUnaryInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (_ interface{}, err error) {
	// Based on raven.RecoveryHandler.
	defer func() {
		if rval := recover(); rval != nil {
			debug.PrintStack()
			rvalStr := fmt.Sprint(rval)
			log.Errorf("Panic calling %s; %s", info.FullMethod, rvalStr)
			packet := raven.NewPacket(rvalStr, raven.NewException(errors.New(rvalStr), raven.NewStacktrace(2, 3, nil)), nil)
			raven.Capture(packet, nil)
			err = status.Errorf(codes.Internal, "%s", rvalStr)
		}
	}()
	return handler(ctx, req)
}

func RecoveryStreamInterceptor(req interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) (err error) {
	// Based on raven.RecoveryHandler.
	defer func() {
		if rval := recover(); rval != nil {
			debug.PrintStack()
			rvalStr := fmt.Sprint(rval)
			log.Errorf("Panic calling %s; %s", info.FullMethod, rvalStr)
			packet := raven.NewPacket(rvalStr, raven.NewException(errors.New(rvalStr), raven.NewStacktrace(2, 3, nil)), nil)
			raven.Capture(packet, nil)
			err = status.Errorf(codes.Internal, "%s", rvalStr)
		}
	}()
	return handler(req, stream)
}
