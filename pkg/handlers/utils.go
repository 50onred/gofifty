package handlers

import (
	"net"
	"net/http"
)

var robotsResponse = []byte("User-agent: *\nDisallow: /\n")
var healthResponse = []byte("OK")

// Health handler returns "OK".
func Health(w http.ResponseWriter, r *http.Request) {
	w.Write(healthResponse)
}

// Robots handler returns robots.txt content that disallows all robots.
func Robots(w http.ResponseWriter, r *http.Request) {
	w.Write(robotsResponse)
}

// RegisterUtils registers Health, Robots and Expvar handlers.
func RegisterUtils(r *http.ServeMux) {
	r.HandleFunc("/health", Health)
	r.HandleFunc("/robots.txt", Robots)
	r.HandleFunc("/debug/vars", Expvar)
}

// OnlyLocalhost wrapper disallows all requests that come from IP addresses other than 127.0.0.1 and ::1.
func OnlyLocalhost(nextFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if ipStr, _, _ := net.SplitHostPort(r.RemoteAddr); ipStr != "127.0.0.1" && ipStr != "::1" {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		nextFunc(w, r)
	}
}

// RemoveExtraHeaders wrapper removes "Content-Type" and "Date" headers.
func RemoveExtraHeaders(nextFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		headers := w.Header()
		headers["Content-Type"] = nil
		headers["Date"] = nil
		nextFunc(w, r)
	}
}
