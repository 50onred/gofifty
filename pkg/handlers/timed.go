package handlers

import (
	"bitbucket.org/50onred/gofifty/pkg/statsd_extended"
	"fmt"
	"github.com/DataDog/datadog-go/statsd"
	"net/http"
)

// Timed wrapper sends HTTP endpoint timing to DogStatsD.
func TimedEndpoint(g *statsd.Client, name string, nextFunc http.HandlerFunc) http.HandlerFunc {
	return TimedSampledEndpoint(g, name, 1, nextFunc)
}

func TimedSampledEndpoint(g *statsd.Client, name string, rate float64, nextFunc http.HandlerFunc) http.HandlerFunc {
	tags := []string{fmt.Sprintf("endpoint_name:%s", name)}
	return func(w http.ResponseWriter, r *http.Request) {
		statsd_extended.TimedSampled(g, "endpoint.request", tags, rate, func() {
			nextFunc(w, r)
		})
	}
}
