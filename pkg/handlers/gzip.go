package handlers

import (
	"compress/gzip"
	"github.com/getsentry/raven-go"
	"net/http"
)

// GunzipBody handler decodes gzip encoded body. Add gzip=0 parameter to the query string to disable the handler.
func GunzipBody(nextFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.FormValue("gzip") == "0" {
			nextFunc(w, r)
			return
		}
		reader, err := gzip.NewReader(r.Body)
		if err != nil {
			raven.CaptureError(err, nil, raven.NewHttp(r))
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		originalBody := r.Body
		r.Body = reader
		nextFunc(w, r)
		originalBody.Close()
	}
}
