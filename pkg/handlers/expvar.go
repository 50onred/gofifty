package handlers

import (
	"expvar"
	"net/http"
)

// Expvar calls the original expvar HTTP handler, but allows incoming requests only from localhost.
func Expvar(ow http.ResponseWriter, or *http.Request) {
	OnlyLocalhost(func(w http.ResponseWriter, r *http.Request) {
		expvar.Handler().ServeHTTP(w, r)
	})(ow, or)
}
