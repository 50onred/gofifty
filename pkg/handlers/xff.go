package handlers

import (
	"net/http"

	"github.com/sebest/xff"
)

// XFF wrapper replaces the RemoteAddr with a value parsed from the X-Forwarded-For HTTP header.
func XFF(nextFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.RemoteAddr = xff.GetRemoteAddr(r)
		nextFunc(w, r)
	}
}
