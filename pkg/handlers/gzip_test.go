package handlers

import (
	"bytes"
	"compress/gzip"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestGzipHandler(t *testing.T) {
	buf := &bytes.Buffer{}
	writer := gzip.NewWriter(buf)
	writer.Write([]byte("this is a test"))
	writer.Close()
	r := httptest.NewRequest("POST", "/gzip", bytes.NewReader(buf.Bytes()))
	w := httptest.NewRecorder()
	handler := GunzipBody(func(w http.ResponseWriter, r *http.Request) {
		io.Copy(w, r.Body)
	})
	handler(w, r)
	assert.Equal(t, "this is a test", w.Body.String())
}

func TestGzipHandlerDisable(t *testing.T) {
	r := httptest.NewRequest("POST", "/gzip?gzip=0", strings.NewReader("this is a test"))
	w := httptest.NewRecorder()
	handler := GunzipBody(func(w http.ResponseWriter, r *http.Request) {
		io.Copy(w, r.Body)
	})
	handler(w, r)
	assert.Equal(t, "this is a test", w.Body.String())
}
