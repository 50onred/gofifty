package utils

import (
	"io/ioutil"
	"net/http"
	"time"
)

func GetEC2LocalIP() string {
	defaultIP := "127.0.0.1"
	client := &http.Client{
		Timeout: time.Second * 2,
	}
	resp, err := client.Get("http://169.254.169.254/latest/meta-data/local-ipv4")
	if err != nil || resp.StatusCode != http.StatusOK {
		return defaultIP
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return defaultIP
	}
	return string(bodyBytes)
}
