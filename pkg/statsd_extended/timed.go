package statsd_extended

import (
	"github.com/DataDog/datadog-go/statsd"
	"time"
)

func Timed(g *statsd.Client, name string, tags []string, f func()) error {
	return TimedSampled(g, name, tags, 1, f)
}

func TimedSampled(g *statsd.Client, name string, tags []string, rate float64, f func()) error {
	start := time.Now()
	f()
	elapsed := time.Since(start)
	return g.Timing(name, elapsed, tags, rate)
}

func NewNoopClient() (*statsd.Client, error) {
	return statsd.NewWithWriter(&MockDatadogStatsDWriter{})
}

type MockDatadogStatsDWriter struct{}

func (m *MockDatadogStatsDWriter) Write(data []byte) (n int, err error) {
	return len(data), nil
}

func (m *MockDatadogStatsDWriter) SetWriteTimeout(time.Duration) error {
	return nil
}

func (m *MockDatadogStatsDWriter) Close() error {
	return nil
}
